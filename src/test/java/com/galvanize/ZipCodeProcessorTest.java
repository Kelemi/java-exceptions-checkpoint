package com.galvanize;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ZipCodeProcessorTest {
    Verifier ver = new Verifier();
    ZipCodeProcessor tests;

    @BeforeEach
    void setup(){
        tests = new ZipCodeProcessor(ver);
    }
    // write your tests here
    @Test
    public void tooShort() {
        String EXPECTED = "The zip code you entered was the wrong length.";
        String actual = tests.process("0000");

        assertEquals(EXPECTED, actual);
    }
    @Test
    public void tooLong() {
        String EXPECTED = "The zip code you entered was the wrong length.";
        String actual = tests.process("000000000000");
        assertEquals(EXPECTED, actual);
    }
    @Test
    public void noService(){
        String EXPECTED = "We're sorry, but the zip code you entered is out of our range.";
        String actual = tests.process("10000");
        assertEquals(EXPECTED, actual);
    }
    @Test
    public void serviceAccepted(){
        String EXPECTED = "Thank you!  Your package will arrive soon.";
        String actual = tests.process("80000");
        assertEquals(EXPECTED, actual);
    }
}