package com.galvanize;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class VerifierTest {
    Verifier ver;
    InvalidFormatException iFE;
    InvalidFormatException lIFE;
    NoServiceException nSE;
    @BeforeEach
    void setUp() {
        ver = new Verifier();
    }
    @Test
    void verifyIFE() throws Exception{
        InvalidFormatException e =
        assertThrows(InvalidFormatException.class, ()->{
            ver.verify("0000");
        });
        assertEquals("ERRCODE 22: INPUT_TOO_SHORT", e.getMessage());
    }
    @Test
    void verifylIFE() throws Exception{
        InvalidFormatException e=
        assertThrows(InvalidFormatException.class, ()->{
            ver.verify("000000000");
        });
        assertEquals("ERRCODE 21: INPUT_TOO_LONG", e.getMessage());
    }
    @Test
    void verifyNSE() throws Exception{
        NoServiceException e =
        assertThrows(NoServiceException.class, ()->{
            ver.verify("10000");
        });
        assertEquals("ERRCODE 27: NO_SERVICE", e.getMessage());
    }
}